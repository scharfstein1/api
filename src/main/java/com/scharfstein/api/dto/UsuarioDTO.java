package com.scharfstein.api.dto;

import javax.validation.constraints.NotBlank;

public class UsuarioDTO {
	
	@NotBlank(message = "username es onligatorio")
	private String username;
	@NotBlank(message = "password es onligatorio")
	private String password;
	
	public UsuarioDTO() { }
	
	public UsuarioDTO(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
