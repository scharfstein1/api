package com.scharfstein.api.dto;

public class AutenticarResponseDTO {
	
    private String token;
    
    public AutenticarResponseDTO() {}
    
	public AutenticarResponseDTO(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
    
}
