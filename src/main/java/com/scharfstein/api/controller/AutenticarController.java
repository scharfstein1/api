package com.scharfstein.api.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.security.auth.message.AuthException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.scharfstein.api.dto.AutenticarResponseDTO;
import com.scharfstein.api.dto.UsuarioDTO;
import com.scharfstein.api.entity.Usuario;
import com.scharfstein.api.service.AutenticarService;


@RestController
@RequestMapping("/autenticar")
@CrossOrigin(origins = "*", methods= {RequestMethod.POST})
public class AutenticarController {
	
	@Autowired
	private AutenticarService autenticarService;

	/**
	 * Valida un usuario
	 * Post
	 * @return boolean
	 */
	@PostMapping
	public AutenticarResponseDTO autenticar(@RequestBody UsuarioDTO usuario){
		
		try {
			String token = autenticarService.auth(usuario);

			return new AutenticarResponseDTO(token);
					
		}catch(EntityNotFoundException e) {
			throw new ResponseStatusException(
			  HttpStatus.NOT_FOUND, "Usuario no encontrado"
			);
		}catch(AuthException e) {
			throw new ResponseStatusException(
			 HttpStatus.UNAUTHORIZED, "Credenciales invalidas"
			);
		}
	}

	
	

}
