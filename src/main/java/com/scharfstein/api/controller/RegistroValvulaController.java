package com.scharfstein.api.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scharfstein.api.entity.RegistroValvula;
import com.scharfstein.api.service.RegistroValvulaService;

@RestController
@RequestMapping("/registro/valvula")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class RegistroValvulaController {
	
	@Autowired
	private RegistroValvulaService registroValvulaService;

	/**
	 * Retorna todas las aperturas de valvulas
	 * GET 
	 * @return List<Temperatura>
	 */
	@GetMapping
	public List<RegistroValvula> get(@Valid @RequestParam(value = "tipo") String tipo){
		
		return registroValvulaService.get(tipo);
		
	}
	
	/**
	 * Guarda una temperatura
	 * POST
	 * @return Temperatura
	 */
	@PostMapping	
	public RegistroValvula store(@Valid @RequestBody RegistroValvula registroValvula) {		
		
		return registroValvulaService.store(registroValvula);
		
	}
	
	

}
