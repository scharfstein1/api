package com.scharfstein.api.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scharfstein.api.entity.RegistroSensor;
import com.scharfstein.api.service.RegistroSensorService;

@RestController
@RequestMapping("/registro/sensor")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class RegistroSensorController {
	
	@Autowired
	private RegistroSensorService registroSensorService;

	/**
	 * Retorna todas las temperaturas
	 * GET
	 * @return List<Temperatura>
	 */
	@GetMapping
	public List<RegistroSensor> get(@Valid @RequestParam(value = "tipo") String tipo){
		
		return registroSensorService.get(tipo);
		
	}
	
	/**
	 * Guarda una temperatura
	 * POST
	 * @return Temperatura
	 */
	@PostMapping	
	public RegistroSensor store(@Valid @RequestBody RegistroSensor registroSensor) {		
		
		return registroSensorService.store(registroSensor);
		
	}
	
	

}
