package com.scharfstein.api.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.scharfstein.api.dto.UsuarioDTO;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtTokenService {
	
	private final String secret = "scharfstein";	
	private final int expiracion = 38000;

	
	public String generar(UsuarioDTO usuario) {
		
		return Jwts.builder()
				.setSubject(usuario.getUsername())
				.claim("username", usuario.getUsername())
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + expiracion * 1000))
				.signWith(SignatureAlgorithm.HS512, secret)
				.compact();
	}
	
}
