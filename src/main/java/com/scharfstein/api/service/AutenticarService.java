package com.scharfstein.api.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.security.auth.message.AuthException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.scharfstein.api.dto.UsuarioDTO;
import com.scharfstein.api.entity.Usuario;
import com.scharfstein.api.repository.UsuarioRepository;


@Service
public class AutenticarService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	JwtTokenService jwtTokenService;

	public String auth(UsuarioDTO usuarioDTO) throws AuthException {
		
		Usuario usuario =  usuarioRepository.buscarPorUsername(usuarioDTO.getUsername());
		
		if(usuario == null) {
			throw new EntityNotFoundException("Usuario no encontrado");
		}
		
		if(!passwordEncoder.matches(usuarioDTO.getPassword(), usuario.getPassword())) {
			throw new AuthException("Contraseña incorrecta");
		}
		
		return jwtTokenService.generar(usuarioDTO);
		
		
	}
	
}
