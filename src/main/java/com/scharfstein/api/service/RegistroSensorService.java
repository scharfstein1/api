package com.scharfstein.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.scharfstein.api.entity.RegistroSensor;
import com.scharfstein.api.repository.RegistroSensorRepository;
import com.scharfstein.api.specification.RegistroSensorSpecifications;

@Service
public class RegistroSensorService {
	
	@Autowired
	RegistroSensorRepository registroSensorRepository;	
	
	public RegistroSensor store(RegistroSensor registroSensor) {
		return registroSensorRepository.save(registroSensor);
	}
	
	public List<RegistroSensor> get(String tipo) {
		
		Specification<RegistroSensor> spec = Specification.where(
				RegistroSensorSpecifications.filtrarPorTipo(tipo)
		);
		
		
		return registroSensorRepository.findAll(spec);
	}
	
}
