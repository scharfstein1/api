package com.scharfstein.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.scharfstein.api.entity.RegistroValvula;
import com.scharfstein.api.repository.RegistroValvulaRepository;
import com.scharfstein.api.specification.RegistroValvulaSpecifications;

@Service
public class RegistroValvulaService {
	
	@Autowired
	RegistroValvulaRepository registroValvulaRepository;	

	public RegistroValvula store(RegistroValvula registroSensor) {
		return registroValvulaRepository.save(registroSensor);
	}
	
	public List<RegistroValvula> get(String tipo) {
		Specification<RegistroValvula> spec = Specification.where(
				RegistroValvulaSpecifications.filtrarPorTipo(tipo)
		);
		
		
		return registroValvulaRepository.findAll(spec);
	}
	
}
