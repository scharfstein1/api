package com.scharfstein.api.specification;

import org.springframework.data.jpa.domain.Specification;

import com.scharfstein.api.entity.RegistroSensor;

public class RegistroSensorSpecifications {
	
	public static Specification<RegistroSensor> filtrarPorTipo(String tipo) {
	    return (registro, cq, cb) -> cb.equal(registro.get("tipo"), tipo);
	}

	
}
