package com.scharfstein.api.specification;

import org.springframework.data.jpa.domain.Specification;
import com.scharfstein.api.entity.RegistroValvula;

public class RegistroValvulaSpecifications {
	
	public static Specification<RegistroValvula> filtrarPorTipo(String tipo) {
	    return (registro, cq, cb) -> cb.equal(registro.get("tipo"), tipo);
	}

	
}
