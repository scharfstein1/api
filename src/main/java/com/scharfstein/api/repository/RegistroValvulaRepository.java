package com.scharfstein.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.scharfstein.api.entity.RegistroValvula;

@Repository
public interface RegistroValvulaRepository extends JpaRepository<RegistroValvula, Long>, JpaSpecificationExecutor<RegistroValvula>{

}
