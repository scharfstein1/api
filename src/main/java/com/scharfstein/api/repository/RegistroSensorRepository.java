package com.scharfstein.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.scharfstein.api.entity.RegistroSensor;
import com.scharfstein.api.specification.RegistroSensorSpecifications;

@Repository
public interface RegistroSensorRepository extends JpaRepository<RegistroSensor, Long>, JpaSpecificationExecutor<RegistroSensor>{
	
}
