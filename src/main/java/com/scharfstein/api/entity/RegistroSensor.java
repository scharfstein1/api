package com.scharfstein.api.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.*;

import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "registro_sensor")
public class RegistroSensor {

	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	@NotBlank(message = "Tipo es obligatorio")
	private String tipo;
	
	@Column(nullable = false)
	@NotNull(message = "Valor es obligatorio")
	private Double valor;
	
	@Column(name = "creado")
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)	
	private Date creado;
	
	public RegistroSensor() { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}	
	
	
	
}
