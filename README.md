Api

- Requerimientos
    - Java 11
    - PostgreSQL
        - Configuracion de base de datos:
            Ver repositorio de base de datos: https://gitlab.com/scharfstein1/sql
- Instalacion
    - Descargar release desde:
        https://gitlab.com/scharfstein1/api/-/blob/master/releases/v1.0.zip
    - Descomprimir    
    - Configuracion de servidor
        - Configurar el archivo application.yml con las credenciales de base de datos correspondientes
    - Iniciar servidor
        - Abrir el archivo start.bat o en un terminal: java -jar api-0.0.1-SNAPSHOT.jar --spring.config.location=application.yml
